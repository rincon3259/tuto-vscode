El poema (del latín poema, y este del griego ποίημα, «creación»)1​ es una obra de poesía normalmente
en verso,2​ esté o no sujeto a los recursos poéticos clásicos de la métrica, el ritmo y la rima.

También hay poemas en prosa (prosa poética, poema en prosa). El poema largo puede dividirse en «cantos»,
y uno breve en estrofas y soneto. El conjunto de poemas es un poemario (libro de poemas o recopilación de
poemas). Es muy habitual hacer antologías de poemas3​ y competencias de poemas (juegos florales).

Joachim du Bellay, en Défensa et illustratcion de la lengua francesa (Defensa e ilustración de la lengua
francesa, 1549), define el poema con una «ouvrage en vers d'une assez grande étendue» (‘’obra en verso
de una extensión bastante grande’’).

El poema sinfónico es una composición musical para orquesta, de forma libre y desarrollo sugerido por una
idea poética u obra literaria.4

estoy retomando mi estudio despues de 8 meses.
